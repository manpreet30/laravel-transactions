<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $transactions = Transaction::listTransactioins();

        if ($request->ajax()) {
            return view('transactions.index-ajax')->with(compact('transactions'));
        }
        return view('transactions.index')->with(compact('transactions'));
    }

    public function create()
    {
        return view('transactions.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:1,2',
            'description' => 'required|max:250',
            'amount' => 'required|numeric|min:0|max:9999999',
        ]);

        Transaction::createTransaction($request);

        return redirect()->route('transactions.index')->with('success', 'Transaction created successfully.');
    }

}
