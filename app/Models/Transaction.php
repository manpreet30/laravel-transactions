<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const CREDIT = 1;
    const DEBIT = 1;

    protected $fillable = ['type', 'amount', 'description', 'balance'];

    public static function listTransactioins()
    {
        return static::orderBy('id', 'DESC')->get();
    }

    public static function createTransaction($request)
    {
        $last_trans = static::select('balance')->orderBy('id', 'DESC')->first();
        $balance = $last_trans ? $last_trans->balance : 0;

        if($request->input('type') == static::CREDIT) {
            $balance += $request->input('amount');
        } else {
            $balance -= $request->input('amount');
        }
        
        $request->merge(['balance' => $balance]);

        Transaction::create($request->all());

    }

}
