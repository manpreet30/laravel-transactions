<?php

return [
    'transactions' => 'Transactions',
    'transaction' => 'Transaction',
    'add_transaction' => 'Add Transaction',
    'balance' => 'Balance',
    'amount' => 'Amount',
    'date' => 'Date',
    'description' => 'Description',
    'debit' => 'Debit',
    'credit' => 'Credit',
    'submit' => 'Submit',
    'type' => 'Type',
    'select_type' => 'Select Type',
];
