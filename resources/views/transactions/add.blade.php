@extends('welcome')
@section('content')
  <div class="container">
  <div class="row">
        <div class="col-12 text-right">
          <a class="btn btn-success" href="{{ route('transactions.index') }}">{{ trans('general.transactions') }}</a>
        </div>
      </div>
      <br>
  <div class="row">
    <div class="col-6">
      @if ( session()->has('success') )
          <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
      @endif
      <h2 class="">
          {{ trans('general.add_transaction') }}
      </h2>
      <form method="POST" action="{{ route('transactions.store') }}">
        @csrf
        <div class="form-group">
          <label for="type">{{ trans('general.type') }}*</label>
          <select class="custom-select" name="type" id="type" required>
            <option value="">{{ trans('general.select_type') }}</option>
              <option value="1" @if(old('type') == 1) selected @endif >Credit</option>
              <option value="2" @if(old('type') == 2) selected @endif >Debit</option>
          </select>
          @error('type')
          <div class="alert alert-danger" role="alert">
             {{ $message }}
          </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="amount">{{ trans('general.amount') }}*</label>
          <input type="text" class="form-control" id="amount" value="{{ old('amount') }}" required name="amount" placeholder="{{ trans('general.amount') }}">
          @error('amount')
          <div class="alert alert-danger" role="alert">
             {{ $message }}
          </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="description">{{ trans('general.description') }}*</label>
          <textarea class="form-control" id="description" required name="description" placeholder="{{ trans('general.description') }}">{{ old('description') }}</textarea>
          @error('description')
          <div class="alert alert-danger" role="alert">
             {{ $message }}
          </div>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary">{{ trans('general.submit') }}</button>
      </form>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="/js/transactions.js"></script>
@endsection
