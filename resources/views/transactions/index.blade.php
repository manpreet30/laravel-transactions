  @extends('welcome')
  @section('content')
  <div class="container">
  <div class="row">
    <div class="col-10">
      <h2 class="">
          {{ trans('general.transactions') }}
      </h2>
      @if ( session()->has('success') )
          <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
      @endif
      <div class="row">
        <div class="col-12 text-right">
          <a class="btn btn-success" href="{{ route('transactions.create') }}">{{ trans('general.add_transaction') }}</a>
        </div>
      </div>
      <br>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">{{ trans('general.date') }}</th>
            <th scope="col">{{ trans('general.description') }}</th>
            <th scope="col">{{ trans('general.credit') }}</th>
            <th scope="col">{{ trans('general.debit') }}</th>
            <th scope="col">{{ trans('general.balance') }}</th>
            <th scope="col">&nbsp;</th>
          </tr>
        </thead>
        <tbody id="table-row-wrapper">
          @foreach($transactions as $row)
            <tr>
              <th>{{ $row->created_at }}</th>
              <th>{{ $row->description }}</th>
              @if($row->type == 1)
                <th>{{ $row->amount }}</th>
                <th></th>
              @else
                <th></th>
                <th>-{{ $row->amount }}</th>
              @endif
              <th>{{ $row->balance }}</th>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="/js/transactions.js"></script>
@endsection
