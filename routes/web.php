<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/add', 'App\Http\Controllers\TransactionController@create')->name('transactions.create');
Route::post('/add', 'App\Http\Controllers\TransactionController@store')->name('transactions.store');
Route::get('/', 'App\Http\Controllers\TransactionController@index')->name('transactions.index');

